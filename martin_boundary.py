from numpy import exp, arange
import numpy as np
from pylab import meshgrid, cm, imshow, contour, clabel, colorbar, axis, title, show


# the function that I'm going to plot
def z_func(x, y):
    sigma = np.array([[0.6, 0],
                     [0, 0.8]])
    mu = np.array([0.06, 0.06])

    b_combo = np.array([[[ptx, pty] for ptx in x] for pty in y])
    result = [[0.5 * np.dot(np.dot(b_combo[i, j], sigma), b_combo[i, j]) + np.dot(mu, b_combo[i, j]) - 0.06
               for i in range(0, b_combo.shape[0])]
              for j in range(0, b_combo.shape[1])]
    return result


x = arange(-3.0, 3.0, 0.1)
y = arange(-3.0, 3.0, 0.1)
X, Y = meshgrid(x, y)  # grid of point
Z = z_func(x, y)  # evaluation of the function on the grid

im = imshow(Z, cmap=cm.RdBu)  # drawing the function
# adding the Contour lines with labels
cset = contour(Z, arange(-1, 1.5, 0.2), linewidths=2, cmap=cm.Set2)
clabel(cset, inline=True, fmt='%1.1f', fontsize=10)
colorbar(im)  # adding the colobar on the right
# latex fashion title
title('Martin Boundary')
show()