import numpy as np
import datetime as dt
import pandas as pd
import matplotlib.pyplot as plt


def generate_random(batch_size=None, dimension=None, distribution=None):
    print(dimension)
    print(distribution)
    return batch_size


def get_distribution(dimension):
    return dimension


if __name__ == '__main__':
    n = 5
    dim = 1

    mean = [0.06, 0.06, 0.06]
    cov = [[0.6, 0, 0], [0, 0.8, 0], [0, 0, 0.8]]
    cov2 = np.identity(3)
    test = np.random.multivariate_normal(mean, cov, size=5000)
    test = np.random.multivariate_normal(mean, cov, size=5000)
    x, y, z = np.random.default_rng().multivariate_normal(mean, cov, 1).T
    plt.plot(x, y, 'x')
    plt.axis('equal')
    plt.show()

    # First generate Mean of len n
    mean = np.ones(3)
    cov = np.identity(3)
    random_points = np.random.multivariate_normal(mean, cov, size=5000)

    cov = np.array([[0.6, 0], [0, 0.8]])
    mean = np.array([0.06, 0.06])
    random_points = np.random.multivariate_normal(mean, cov, size=5000)

    # dist = get_distribution(dim)
    # batch = generate_random(batch_size=n, dimension=dim, distribution=dist)