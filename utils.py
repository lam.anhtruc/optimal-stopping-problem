import keras
import tensorflow as tf
import keras.backend as K
from keras.constraints import Constraint
from keras import regularizers
from keras.losses import Loss
import numpy as np


class Between(Constraint):
    def __init__(self, min_value, max_value):
        self.min_value = min_value
        self.max_value = max_value

    def __call__(self, w):
        return K.clip(w, self.min_value, self.max_value)

    def get_config(self):
        return {'min_value': self.min_value,
                'max_value': self.max_value}


class MartinBoundaryRegularizer(regularizers.Regularizer):
    def __init__(self, strength):
        self.strength = strength

    def __call__(self, b_weights):
        sigma = np.array([[0.6, 0],
                          [0, 0.8]])
        mu = np.array([0.06, 0.06])
        max_dist = 0
        for k in range(0, K.get_value(b_weights).shape[1]):
            b = K.get_value(b_weights)[:, k]
            dist = 0.5 * np.dot(np.dot(b, sigma), b) + np.dot(mu, b) - 0.06
            max_dist = dist if dist > max_dist else max_dist
        reg = K.constant(max_dist)

        return self.strength * reg


class LambdaRegularizer(regularizers.Regularizer):
    def __init__(self, strength):
        self.strength = strength

    def __call__(self, lamda):
        max_value = max(0, max(-K.get_value(lamda))[0])
        reg = K.constant(max_value)
        return self.strength * reg


class CustomLoss(Loss):
    def __init__(self):
        super().__init__()

    def call(self, y_true, y_pred):
        epsilon = 0.1
        summ = K.maximum(K.abs(y_true) + K.abs(y_pred) + epsilon, 0.5 + epsilon)
        smape = K.abs(y_pred - y_true) / summ * 2.0
        return smape
