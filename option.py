import numpy as np
import matplotlib.pyplot as plt

"""
Example Usage:

parameter = {"x0": 100,
                 "time_horizon": 0.5,
                 "mean": 0,
                 "std": 0.4,
                 "number_of_steps": 100,
                 "option_style": "put",
                 "strike": 100,
                 "interest_rate": 0.06}
    put_option = {opt: Option(parameter) for opt in range(0, 1000)}
    train_set_x = put_option
"""


class Option:
    def __init__(self, parameter: dict):
        self.underlying_title = "Standard Brownian Motion"
        self.timeline = None
        self.underlying = None
        self.generating_bm(parameter)
        self.option_title = None
        self.option_process = None
        self.generating_option(parameter)



    def generating_bm(self, parameter):
        """Arithmetic Brownian Motion"""
        x0 = parameter["x0"]
        T = parameter["time_horizon"] * 365
        mu = parameter["mean"]
        sigma = parameter["std"]
        N = parameter["number_of_steps"]
        r = parameter["interest_rate"]
        dt = T / N
        t = np.linspace(0, T, N)
        W = np.random.normal(loc=mu, scale=sigma, size=N)
        W = np.cumsum(W) * sigma * np.sqrt(dt) + r*dt
        W = W - W[0] + x0
        self.timeline = t
        self.underlying = W
        return t, W

    def generating_option(self, parameter):
        option_style = parameter["option_style"]
        strike = parameter["strike"]
        interest_rate = parameter["interest_rate"]
        T = parameter["time_horizon"] * 365
        N = parameter["number_of_steps"]
        dt = T / N
        if option_style == "put":
            self.put_option(strike, interest_rate, dt)
        elif option_style == "digital":
            self.digital_option()

    def put_option(self, K: float, r: float, dt: float):
        self.option_process = np.where(self.underlying > K, 0, K - self.underlying)
        self.option_process = np.exp(-r * dt) * self.option_process
        self.option_title = f"Put With Strike {K}"
        return self.option_process

    def digital_option(self):
        pass

    def show_processes(self):
        figure, (ax1, ax2) = plt.subplots(2)
        ax1.plot(self.timeline, self.underlying)
        ax1.set_title(self.underlying_title, y=1.0, pad=-14)
        ax2.plot(self.timeline, self.option_process)
        ax2.set_title(self.option_title, y=1.0, pad=-14)
        plt.show()

    def v_process(self, i):
        return self.option_process[i]
