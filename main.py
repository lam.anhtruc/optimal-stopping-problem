import pandas as pd
import numpy as np
import keras
import keras.backend as K
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import SGD
from utils import Between
from utils import MartinBoundaryRegularizer
from utils import LambdaRegularizer
from utils import CustomLoss


import tensorflow as tf
from option import Option
import generate_random_points


def train_model():
    # Example from Christensen
    # Bei Strike 80 ist v(x) = 21.606
    mean = np.array([0.06, 0.06])
    cov = np.array([[0.6, 0], [0, 0.8]])

    # SAMPLE SIZE
    SIZE = 500
    BATCH_SIZE = 10
    EPOCH = SIZE // BATCH_SIZE
    random_points = np.random.multivariate_normal(mean, cov, size=SIZE).astype("float32")
    target = np.array([[0, 0] for k in range(0, SIZE)]).astype("float32")

    # N shall be the size of the Hidden Layer
    N = 3
    dimension = 2

    # Model
    model = Sequential()
    model.add(Dense(N, input_dim=dimension,
                    bias_constraint=Between(0, 0),
                    kernel_regularizer=MartinBoundaryRegularizer(1),
                    activation='exponential'))
    model.add(Dense(1, bias_constraint=Between(0, 0),
                    kernel_regularizer=LambdaRegularizer(1)))

    sgd = SGD(lr=0.01)

    model.compile(loss=custom_loss, optimizer=sgd, metrics=['accuracy'])
    history = model.fit(random_points, target, epochs=EPOCH, batch_size=BATCH_SIZE, verbose=1)

    # model.layers[0].get_weights()[0] Weight of the Hidden Layer
    print(model.layers[0].get_weights()[0])
    # model.layers[1].get_weights()[0] Weight of the Hidden Layer
    print(model.layers[1].get_weights()[0])
    # model.layers[0].get_weights()[1] Bias of the Hidden Layer


def custom_loss(random_point, target):
    b_sum = tf.reduce_sum(random_point)
    cl = tf.square(b_sum-target)
    return cl


if __name__ == '__main__':
    train_model()

    # input = put_option[0].timeline
    # first_layer = sum(input * b)
    # np.exp(first_layer)
    # len(input)

    # def objective_function(dt, lam, b, x):
    #   N = round(T/dt)
    #   t = np.linspace(0, T, N)
    #   sum = sum(lam * np.exp(x * b))
    #   return sum


