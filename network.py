import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import math


class Network(nn.Module):
    def __init__(self, input_size=None, hidden_size=None):
        super().__init__()
        # Inputs to hidden layer linear transformation
        self.hidden = nn.Linear(input_size, hidden_size)
        # Output layer, 10 units - one for each digit
        self.output = nn.Linear(hidden_size, 1)

        # Define sigmoid activation and softmax output
        self.sigmoid = nn.Sigmoid()
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        # Pass the input tensor through each of our operations
        x = self.hidden(x)
        # exponential activation function

        x = self.sigmoid(x)
        x = torch.exp(x)
        x = self.softmax(x)

        return x


def train_model():
    dimension = 2
    hidden_layer_size = 2
    model = Network(dimension, hidden_layer_size)


if __name__ == '__main__':
    train_model()
